from openerp import models, fields, api, _
import logging
# Logger for debug
_logger = logging.getLogger(__name__)


#############
# Ups #
#############
class DMUps(models.Model):
    _name = 'dm.ups'
    _description = 'UPS Systems'

# -- Check if np id is required
    @api.multi
    def _get_np_required(self):
        params = self._context.get('params')
        if params:
            np_required = False if 'action' in params else True
        else:
            np_required = True
        return np_required

    # Fields
    parent_id = fields.Many2one(string="Parent ups", comodel_name='dm.ups')
    parent_variant = fields.Many2one(string="Parent ups variant", comodel_name='dm.ups.variant',
                                     domain="[('ups','=', parent_id)]")
    children = fields.One2many(string="Child upss", comodel_name='dm.ups', inverse_name='parent_id')
    children_count = fields.Integer(string="Children count", compute="count_children")
    # Product
    product = fields.Many2one(string="Related product", comodel_name='product.product')
    product_name = fields.Char(string="Name", related='product.name')
    # Name
    name = fields.Char(string="Name", compute='compose_name')
    name_draft = fields.Char(string="Name (no product)", translate=True)

    # Dimensions
    length = fields.Integer(string="Length(mm)", required=True)
    width = fields.Integer(string="Width(mm)", required=True)
    height = fields.Integer(string="Height(mm)", required=True)
    weight = fields.Integer(string="Weight(kg)", required=True)
    
    # Temperatures
    ip_class = fields.Many2one(string="IP protection class", comodel_name='dm.product.ip')
    temp_min = fields.Integer(string="Working temperature, min(C)", required=True)
    temp_max = fields.Integer(string="Working temperature, max(C)", required=True)

    # Electrical data
    time_standby = fields.Integer(string="Standby time,minutes", required=True)
    phase_qty = fields.Integer(string="Phase qty", default=3)
    voltage_in_min = fields.Integer(string="Input voltage, min(V)", required=True, default=380)
    voltage_in_max = fields.Integer(string="Input voltage, max(V)", required=True, default=380)
    frequency_min = fields.Integer(string="Input frequency, min(Hz)", required=True, default=50)
    frequency_max = fields.Integer(string="Input frequency, max(Hz)", required=True, default=50)
    voltage_out = fields.Integer(string="Output voltage(V)", required=True, default=220)
    amp = fields.Integer(string="Current (Amp)")
    kw = fields.Float(string="Power (kW)", digits=(6, 2), compute='_compute_kw')

    # Request for new product
    np_id = fields.Many2one(string="Product design request id", comodel_name='dm.bp.np')
    np_required = fields.Boolean(string="Specify product design request", compute="_dummy",
                                 default=_get_np_required)
    # Documents
    documents = fields.Many2many(string="Documents", comodel_name='ir.attachment',
                                 compute="get_documents",
                                 inverse="set_documents")
    # Appius integration
    appius_id = fields.Char(string="Appius ID")

    # Notes
    note = fields.Text(string="Note", translate=True)
    # Variants
    variants = fields.One2many(string="Variants", comodel_name='dm.ups.variant',
                               inverse_name='ups')
    variants_count = fields.Integer(string="Variants count", compute="count_variants")

    # Order
    _order = 'voltage_out, amp'

    # SQL constraints
    _sql_constraints = [('ups_product_unique',
                         'UNIQUE (product)',
                         _('This ups already exists!'))]

# -- Get documents
    @api.multi
    def get_documents(self):
        for rec in self:
            rec.documents = self.env['ir.attachment'].search(['&', ("res_model", '=', 'dm.ups'),
                                                              ('res_id', '=', rec.id)])

# -- Set documents
    @api.multi
    def set_documents(self):
        for rec in self:
            for document in rec.documents:
                if not document.res_id:
                    document.write({'res_model': 'dm.ups', 'res_id': rec.id})

# -- Name compose -- #
    @api.multi
    def compose_name(self):
        for rec in self:
            rec.name = rec.product_name if rec.product else rec.name_draft

# -- Dummy
    def _dummy(self):
        return

# -- Create -- #
    @api.model
    def create(self, vals):
        # Check if created from Request for new product add ref to np_id
        np_id = False
        if 'np_id' in vals:
            np_id = vals['np_id']

        # If has cargo lines set ups
        has_lines = True if 'cargo_lines' in vals else False

        # Create
        rec = super(DMUps, self).create(vals)

        # Update lines
        if has_lines:
            for line in rec.cargo_lines:
                line.ups = rec.id

        # Create NP line
        if np_id:
            ref = rec._name + "," + str(rec.id)
            self.env['dm.bp.np.line'].create({'np_id': np_id, 'dm_product_ref': ref})

        return rec

# -- Compute kw -- #
    @api.depends('amp', 'voltage_out')
    @api.multi
    def _compute_kw(self):
        for rec in self:
            rec.kw = (rec.amp * rec.voltage_out / 1000.00) if rec.amp else 0


# -- Count variants
    @api.multi
    def count_variants(self):
        for rec in self:
            rec.variants_count = self.env['dm.ups.variant'].search_count([('ups', '=', rec.id)])

# -- Open variants
    @api.multi
    def open_variants(self):
        self.ensure_one()
        current_ups_id = self.id
        context = {
            'default_ups': current_ups_id
        }

        tree_view_id = self.env['ir.ui.view'].search([('name', '=', 'dm.ups.variant.tree')]).id

        # Compose view header
        header = _("Variants of ") + self.name

        return {
            'name': header,
            "views": [[tree_view_id, "tree"], [False, "form"]],
            'res_model': 'dm.ups.variant',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'view_id': tree_view_id,
            'context': context,
            'domain': [('ups', '=', current_ups_id)]
        }

# -- Count children
    @api.multi
    def count_children(self):
        for rec in self:
            rec.children_count = self.env['dm.ups'].search_count([('parent_id', '=', rec.id)])

# -- Open children
    @api.multi
    def open_children(self):
        self.ensure_one()
        current_ups_id = self.id
        context = {
            'default_parent_id': current_ups_id
        }

        tree_view_id = self.env['ir.ui.view'].search([('name', '=', 'dm.ups.tree')]).id

        # Compose view header
        header = _("Children of ") + self.name

        return {
            'name': header,
            "views": [[tree_view_id, "tree"], [False, "form"]],
            'res_model': 'dm.ups',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'view_id': tree_view_id,
            'context': context,
            'domain': [('parent_id', '=', current_ups_id)]
        }


#####################
# Ups variant #
#####################
class DMUpsVariant(models.Model):
    _name = 'dm.ups.variant'
    _description = 'UPS system variant'

# -- Check if np id is required
    @api.multi
    def _get_np_required(self):
        params = self._context.get('params')
        if params:
            np_required = False if 'action' in params else True
        else:
            np_required = True
        return np_required

    # Fields
    parent_id = fields.Many2one(string="Parent variant", comodel_name='dm.ups.variant',
                                domain="[('ups','=', ups)]")
    has_parent = fields.Boolean(string="No parent", compute='check_parent')
    # Ups
    ups = fields.Many2one(string="Ups", comodel_name='dm.ups', required=True)
    # Product
    product = fields.Many2one(string="Related product", comodel_name='product.product')
    product_name = fields.Char(string="Name", related='product.name')
    # Name
    name = fields.Char(string="Name", compute='compose_name')
    name_draft = fields.Char(string="Name (no product)")
    length = fields.Integer(string="Length(mm)")
    width = fields.Integer(string="Width(mm)", required=True)
    height = fields.Integer(string="Height(mm)", required=True)
    weight = fields.Integer(string="Weight(kg)", required=True)
    # Temperatures
    ip_class = fields.Many2one(string="IP protection class", comodel_name='dm.product.ip')
    temp_min = fields.Integer(string="Working temperature, min(C)", required=True)
    temp_max = fields.Integer(string="Working temperature, max(C)", required=True)
    # Electrical data
    voltage_in_min = fields.Integer(string="Input voltage, min(V)", required=True, default=380)
    voltage_in_max = fields.Integer(string="Input voltage, max(V)", required=True, default=380)
    frequency_min = fields.Integer(string="Input frequency, min(Hz)", required=True, default=50)
    frequency_max = fields.Integer(string="Input frequency, max(Hz)", required=True, default=50)
    # Request for new product
    np_id = fields.Many2one(string="Product design request id", comodel_name='dm.bp.np')
    np_required = fields.Boolean(string="Specify product design request", compute="_dummy",
                                 default=_get_np_required)
    # Documents
    documents = fields.Many2many(string="Documents", comodel_name='ir.attachment',
                                 compute="get_documents",
                                 inverse="set_documents")
    # Notes
    note = fields.Text(string="Note", translate=True)

    # Appius integration
    appius_id = fields.Char(string="Appius ID")

    # Order
    _order = 'id'

    # SQL constraints
    _sql_constraints = [('ups_variant_product_unique',
                         'UNIQUE (product)',
                         _('This ups variant_already exists!'))]

# -- Dummy
    def _dummy(self):
        return

# -- Create -- #
    @api.model
    def create(self, vals):
        # Check if created from Request for new product add ref to np_id
        np_id = False
        if 'np_id' in vals:
            np_id = vals['np_id']

        # Create
        rec = super(DMUpsVariant, self).create(vals)

        # Create NP line
        if np_id:
            ref = rec._name + "," + str(rec.id)
            self.env['dm.bp.np.line'].create({'np_id': np_id, 'dm_product_ref': ref})

        return rec

# -- Check parent
    @api.multi
    def check_parent(self):
        has_parent = True if self._context.get('default_ups', False) else False
        for rec in self:
            rec.has_parent = has_parent


# -- Ups changed
    @api.onchange('ups')
    @api.multi
    def ups_change(self):
        self.ensure_one()
        self.name_draft = self.ups.name
        self.weight = self.ups.weight
        self.length = self.ups.length
        self.width = self.ups.width
        self.height = self.ups.height
        self.temp_max = self.ups.temp_max
        self.temp_min = self.ups.temp_min
        self.ip_class = self.ups.ip_class

# -- Get documents
    @api.multi
    def get_documents(self):
        for rec in self:
            rec.documents = self.env['ir.attachment'].search(['&', ("res_model", '=', 'dm.ups.variant'),
                                                              ('res_id', '=', rec.id)])

# -- Set documents
    @api.multi
    def set_documents(self):
        for rec in self:
            for document in rec.documents:
                if not document.res_id:
                    document.write({'res_model': 'dm.ups.variant', 'res_id': rec.id})

# -- Compose name -- #
    @api.multi
    def compose_name(self):
        for rec in self:
            rec.name = rec.product_name if rec.product else rec.name_draft


