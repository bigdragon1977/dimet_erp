from openerp import models, fields, api, _
# import logging
# Logger for debug
# _logger = logging.getLogger(__name__)


#########
# Cargo #
#########
class DMCargo(models.Model):
    _name = 'dm.cargo'

    # Fields
    name = fields.Char(string="Name", required=True, translate=True)
    type = fields.Selection([
        ('h', 'Heap'),
        ('s', 'Solid'),
        ('c', 'Counted'),
    ], default='h', string="Type", required=True)
    density = fields.Integer(string="Cargo density(kg/m3)", required=True)
    # Cargo lines
    cargo_lines = fields.One2many(string="Magnets",
                                  comodel_name='dm.cargo.line', inverse_name='cargo')

    # Order
    _order = 'type, density, name'

    # SQL constraints
    _sql_constraints = [('cargo_density_unique',
                         'UNIQUE (name, type, density)',
                         _('This cargo already exists!'))]


##############
# Cargo Line #
##############
class DMCargoLine(models.Model):
    _name = 'dm.cargo.line'

    # Fields
    # Magnet
    magnet = fields.Many2one(string="Magnet", comodel_name='dm.magnet')
    name = fields.Char(string="Magnet", related='magnet.name')
    magnet_weight = fields.Integer(string="Magnet weight", related='magnet.weight')
    magnet_ed = fields.Integer(string="ED %", related='magnet.ed')
    magnet_kw_cold = fields.Float(string="Magnet power(kW)", related='magnet.kw_cold')
    # Cargo
    cargo = fields.Many2one(string="Cargo", comodel_name='dm.cargo', required=True, auto_join=True)
    cargo_name = fields.Char(string="Cargo", related='cargo.name')
    cargo_density = fields.Integer(string="Cargo density(kg/m3)", related='cargo.density')
    cargo_temp = fields.Integer(string="Cargo temperature (C)", default=False)
    # Max values
    capacity_max = fields.Integer(string="Lifting capacity, max(kg)", required=True)
    volume_max = fields.Float(string="Volume, max(m3)", digits=(6, 3), required=True)
    # Effective values
    capacity_drop = fields.Integer(string="Capacity drop(% of max)", related='magnet.capacity_drop')
    capacity_effective = fields.Integer(string="Lifting capacity, effective(kg)")
    volume_effective = fields.Float(string="Volume, effective(m3)", digits=(6,3))
    # Completeness
    capacity_limit = fields.Integer(string="Capacity limit(kg)", compute='_get_required_values')
    capacity_complete = fields.Integer(string="Capacity complete(% of max)", compute='_get_capacity_complete')
    volume_limit = fields.Float(string="Volume required(m3)", digits=(6, 3), compute='_placeholder')
    volume_complete = fields.Integer(string="Volume complete(% of required)", compute='_get_volume_complete')
    power_overload = fields.Boolean(string="Power exceeds generator limit", compute='_get_power_overload')
    # Magnet adapter
    adapter_id = fields.Integer(string="Adapter", compute='_get_magnet_adapter')
    adapter_name = fields.Char(string="Adapter name", compute='_get_magnet_adapter')
    adapter_weight = fields.Integer(string="Adapter weight", compute='_placeholder')
    # Documents attached
    documents = fields.Many2many(string="Documents", comodel_name='ir.attachment',
                                 compute="_get_documents")

    # Order
    _order = 'capacity_max DESC, magnet ASC, cargo ASC'

    # SQL constraints
    _sql_constraints = [('cargo_line_unique',
                         'UNIQUE (magnet, cargo)',
                         _('This cargo record for this magnet already exists!'))]


# -- Get attachments
    @api.multi
    def _get_documents(self):
        for rec in self:
            if rec.magnet:
                rec.documents = rec.magnet.documents

# -- Get power overload -- #
    @api.multi
    def _get_power_overload(self):
        power_effective = self.env.context.get('power_effective', 0)
        if power_effective == 0:
            return
        for rec in self:
            rec.power_overload = True if rec.magnet_kw_cold > power_effective else False

# -- Compute effective weight and capacity -- #
    @api.onchange('capacity_max')
    @api.multi
    def calculate_capacity_effective(self):
        for rec in self:
            capacity_drop = rec.capacity_drop
            multiplier = (100 - capacity_drop) if capacity_drop < 100 else 0
            rec.capacity_effective = round(float(rec.capacity_max) * multiplier / 100)
            rec.volume_effective = rec.volume_max * multiplier / 100

# -- Compute maximum weight and capacity -- #
    @api.onchange('capacity_effective', 'volume_effective')
    @api.multi
    def calculate_capacity_max(self):
        for rec in self:
            capacity_drop = rec.capacity_drop
            multiplier = (100 - capacity_drop) if capacity_drop < 100 else 0
            rec.capacity_max = round(float(rec.capacity_effective) / multiplier * 100)
            rec.volume_max = rec.volume_effective / multiplier * 100

# -- Get required values -- #
    @api.multi
    def _get_required_values(self):
        capacity_limit = self.env.context.get('capacity_limit', 0)
        for rec in self:
            rec.capacity_limit = capacity_limit
            rec.volume_limit = float(rec.cargo_density * capacity_limit) / 1000000

# -- Get magnet adapter -- #
    @api.multi
    def _get_magnet_adapter(self):
        has_adapter = self.env.context.get('has_adapter', 0)
        if has_adapter == 0:
            return
        for rec in self:
            for magnet in self.env.context.get('magnets', None):
                if rec.id == magnet['cargo_line_id']:
                    rec.adapter_id = magnet['adapter_id']
                    rec.adapter_name = magnet['adapter_name']
                    rec.adapter_weight = magnet['adapter_weight']

# -- Compute capacity completeness -- #
    @api.multi
    def _get_capacity_complete(self):
        has_adapter = self.env.context.get('has_adapter', 0)
        for rec in self:
            if has_adapter == 0:
                total_weight = rec.magnet_weight
            else:
                for magnet in self.env.context.get('magnets', None):
                    if rec.id == magnet['cargo_line_id']:
                        total_weight = rec.magnet_weight + magnet['adapter_weight']

            rec.capacity_complete = round((rec.capacity_max + total_weight)* 100 / rec.capacity_limit) if rec.capacity_limit > 0 else 0

# -- Compute volume completeness -- #
    @api.multi
    def _get_volume_complete(self):
        for rec in self:
            rec.volume_complete = round(rec.volume_effective * 100 / rec.volume_limit) if rec.volume_limit > 0 else 0

# -- Placeholder for effective weight and capacity-- #
    @api.multi
    def _placeholder(self):
        return

# -- Compute volume -- #
    @api.onchange('capacity_max', 'cargo')
    @api.multi
    def _get_capacity_max(self):
        for rec in self:
            rec.volume_max = float(rec.capacity_max)/rec.cargo_density if rec.cargo_density > 0 else 0

# -- Compute weight -- #
    @api.onchange('volume_max', 'cargo')
    @api.multi
    def _get_volume_max(self):
        for rec in self:
            rec.capacity_max = rec.volume_max*rec.cargo_density if rec.volume_max > 0 else 0


######################
# Grapple Cargo Line #
######################
class DMCargoGrapple(models.Model):
    _name = 'dm.cargo.grapple'

    # Fields
    # Grapple
    grapple = fields.Many2one(string="Grapple", comodel_name='dm.grapple')
    name = fields.Char(string="Grapple", related='grapple.name')
    grapple_weight = fields.Integer(string="Grapple weight", related='grapple.weight')
    # Cargo
    cargo = fields.Many2one(string="Cargo", comodel_name='dm.cargo', required=True, auto_join=True)
    cargo_name = fields.Char(string="Cargo", related='cargo.name')
    cargo_density = fields.Integer(string="Cargo density(kg/m3)", related='cargo.density')
    cargo_temp = fields.Integer(string="Cargo temperature (C)", default=False)
    # Max values
    capacity_max = fields.Integer(string="Lifting capacity, max(kg)", required=True)
    volume_max = fields.Float(string="Volume, max(m3)", digits=(6, 3), required=True)
    # Documents attached
    documents = fields.Many2many(string="Documents", comodel_name='ir.attachment',
                                 compute="_get_documents")

    # Order
    _order = 'capacity_max DESC, grapple ASC, cargo ASC'

    # SQL constraints
    _sql_constraints = [('cargo_grapple_unique',
                         'UNIQUE (grapple, cargo)',
                         _('This cargo record for this grapple already exists!'))]


# -- Get attachments
    @api.multi
    def _get_documents(self):
        for rec in self:
            if rec.grapple:
                rec.documents = rec.grapple.documents

# -- Compute volume -- #
    @api.onchange('capacity_max', 'cargo')
    @api.multi
    def _get_capacity_max(self):
        for rec in self:
            rec.volume_max = float(rec.capacity_max)/rec.cargo_density if rec.cargo_density > 0 else 0

# -- Compute weight -- #
    @api.onchange('volume_max', 'cargo')
    @api.multi
    def _get_volume_max(self):
        for rec in self:
            rec.capacity_max = rec.volume_max*rec.cargo_density if rec.volume_max > 0 else 0
