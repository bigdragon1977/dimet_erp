from openerp import models, fields, api, _


###########
# Grapple #
###########
class DMGrapple(models.Model):
    _name = 'dm.grapple'
    _description = 'Grapple/Clamshell bucket'

# -- Check if np id is required
    @api.multi
    def _get_np_required(self):
        params = self._context.get('params')
        if params:
            np_required = False if 'action' in params else True
        else:
            np_required = True
        return np_required

    # Fields
    parent_id = fields.Many2one(string="Parent grapple", comodel_name='dm.grapple')
    parent_variant = fields.Many2one(string="Parent grapple variant", comodel_name='dm.grapple.variant',
                                     domain="[('grapple','=', parent_id)]")
    children = fields.One2many(string="Child grapples", comodel_name='dm.grapple', inverse_name='parent_id')
    children_count = fields.Integer(string="Children count", compute="count_children")
    # Product
    product = fields.Many2one(string="Related product", comodel_name='product.product')
    product_name = fields.Char(string="Name", related='product.name')
    # Name
    name = fields.Char(string="Name", compute='compose_name')
    name_draft = fields.Char(string="Name (no product)", translate=True)
    # Tech specs
    type = fields.Selection([
        ('c', 'Clamshell'),
        ('g', 'Multi tine'),
        ('f', 'Forestry'),
        ('s', 'Custom grapple'),
    ], required=True, string="Type")
    drive_type = fields.Selection([
        ('r', 'Rope'),
        ('h', 'Hydraulic'),
        ('e', 'Electro-hydraulic'),
        ('m', 'Motor driven'),
    ], required=True, string="Drive type")
    orientation = fields.Selection([
        ('l', 'Longitudinal'),
        ('t', 'Transverse'),
    ], string="Orientation")
    tines_count = fields.Integer(string="Tine count", help="Number of tines")
    ropes_count = fields.Integer(string="Rope count", help="Number of ropes")
    ropes_suspension = fields.Integer(string="Ropes, suspension", help="Number of ropes used for suspension")
    ropes_drive = fields.Integer(string="Ropes, drive", help="Number of ropes used for opening/closing")
    has_teeth = fields.Boolean(string="Has teeth", help="If grapple has teeth", default=False)
    is_underwater = fields.Boolean(string="Is underwater", help="Suitable for underwater operations", default=False)

    rope_diameter = fields.Float(string="Ropes diameter(mm)", digits=(6, 2), help="Rope diameter")
    power = fields.Float(string="Power(kW)", digits=(6, 2))

    # Dimensions
    length_min = fields.Integer(string="Length, min(mm)", required=True)
    length_max = fields.Integer(string="Length, max(mm)", required=True)

    width_min = fields.Integer(string="Width, min(mm)", required=True)
    width_max = fields.Integer(string="Width, max(mm)", required=True)

    height_min = fields.Integer(string="Height, min(mm)", required=True)
    height_max = fields.Integer(string="Height, max(mm)", required=True)

    volume = fields.Float(string="Volume, max(m3)", digits=(6, 3), required=True)
    weight = fields.Integer(string="Weight(kg)", required=True)

    # Temperatures
    temp_min = fields.Integer(string="Working temperature, min(C)", required=True)
    temp_max = fields.Integer(string="Working temperature, max(C)", required=True)
    # Power and capacity
    cargo_lines = fields.One2many(string="Lifting capacity",
                                  comodel_name='dm.cargo.grapple', inverse_name='grapple',
                                  auto_join=True)
    # Request for new product
    np_id = fields.Many2one(string="Product design request id", comodel_name='dm.bp.np')
    np_required = fields.Boolean(string="Specify product design request", compute="_dummy",
                                 default=_get_np_required)
    # Documents
    documents = fields.Many2many(string="Documents", comodel_name='ir.attachment',
                                 compute="get_documents",
                                 inverse="set_documents")
    # Appius integration
    appius_id = fields.Char(string="Appius ID")

    # Notes
    note = fields.Text(string="Note", translate=True)
    # Variants
    variants = fields.One2many(string="Variants", comodel_name='dm.grapple.variant',
                               inverse_name='grapple')
    variants_count = fields.Integer(string="Variants count", compute="count_variants")

    # Order
    _order = 'type, weight, volume'

    # SQL constraints
    _sql_constraints = [('grapple_product_unique',
                         'UNIQUE (product)',
                         _('This grapple already exists!'))]


# -- Get documents
    @api.multi
    def get_documents(self):
        for rec in self:
            rec.documents = self.env['ir.attachment'].search(['&', ("res_model", '=', 'dm.grapple'),
                                                              ('res_id', '=', rec.id)])

# -- Set documents
    @api.multi
    def set_documents(self):
        for rec in self:
            for document in rec.documents:
                if not document.res_id:
                    document.write({'res_model': 'dm.grapple', 'res_id': rec.id})


# -- Name compose -- #
    @api.multi
    def compose_name(self):
        for rec in self:
            rec.name = rec.product_name if rec.product else rec.name_draft

# -- Dummy
    def _dummy(self):
        return

# -- Create -- #
    @api.model
    def create(self, vals):
        # Check if created from Request for new product add ref to np_id
        np_id = False
        if 'np_id' in vals:
            np_id = vals['np_id']

        # If has cargo lines set grapple
        has_lines = True if 'cargo_lines' in vals else False

        # Create
        rec = super(DMGrapple, self).create(vals)

        # Update lines
        if has_lines:
            for line in rec.cargo_lines:
                line.grapple = rec.id

        # Create NP line
        if np_id:
            ref = rec._name + "," + str(rec.id)
            self.env['dm.bp.np.line'].create({'np_id': np_id, 'dm_product_ref': ref})
        return rec

# -- Count variants
    @api.multi
    def count_variants(self):
        for rec in self:
            rec.variants_count = self.env['dm.grapple.variant'].search_count([('grapple', '=', rec.id)])

# -- Open variants
    @api.multi
    def open_variants(self):
        self.ensure_one()
        current_grapple_id = self.id
        context = {
            'default_grapple': current_grapple_id
        }

        tree_view_id = self.env['ir.ui.view'].search([('name', '=', 'dm.grapple.variant.tree')]).id

        # Compose view header
        header = _("Variants of ") + self.name

        return {
            'name': header,
            "views": [[tree_view_id, "tree"], [False, "form"]],
            'res_model': 'dm.grapple.variant',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'view_id': tree_view_id,
            'context': context,
            'domain': [('grapple', '=', current_grapple_id)]
        }

# -- Count children
    @api.multi
    def count_children(self):
        for rec in self:
            rec.children_count = self.env['dm.grapple'].search_count([('parent_id', '=', rec.id)])

# -- Open children
    @api.multi
    def open_children(self):
        self.ensure_one()
        current_grapple_id = self.id
        context = {
            'default_parent_id': current_grapple_id
        }

        tree_view_id = self.env['ir.ui.view'].search([('name', '=', 'dm.grapple.tree')]).id

        # Compose view header
        header = _("Children of ") + self.name

        return {
            'name': header,
            "views": [[tree_view_id, "tree"], [False, "form"]],
            'res_model': 'dm.grapple',
            'type': 'ir.actions.act_window',
            'target': 'current',
            'view_id': tree_view_id,
            'context': context,
            'domain': [('parent_id', '=', current_grapple_id)]
        }


###################
# Grapple variant #
###################
class DMGrappleVariant(models.Model):
    _name = 'dm.grapple.variant'
    _description = 'Grapple variant'

# -- Check if np id is required
    @api.multi
    def _get_np_required(self):
        params = self._context.get('params')
        if params:
            np_required = False if 'action' in params else True
        else:
            np_required = True
        return np_required

    # Fields
    parent_id = fields.Many2one(string="Parent variant", comodel_name='dm.grapple.variant',
                                domain="[('grapple','=', grapple)]")
    has_parent = fields.Boolean(string="No parent", compute='check_parent')
    # Grapple
    grapple = fields.Many2one(string="Grapple", comodel_name='dm.grapple', required=True)
    type = fields.Selection(string="Type", related='grapple.type', readonly=True)
    # Product
    product = fields.Many2one(string="Related product", comodel_name='product.product')
    product_name = fields.Char(string="Name", related='product.name')
    # Name
    name = fields.Char(string="Name", compute='compose_name')
    name_draft = fields.Char(string="Name (no product)")
    # Properties
    has_teeth = fields.Boolean(string="Has teeth", help="If grapple has teeth", default=False)
    # Dimensions
    length_min = fields.Integer(string="Length, min(mm)", required=True)
    length_max = fields.Integer(string="Length, max(mm)", required=True)

    width_min = fields.Integer(string="Width, min(mm)", required=True)
    width_max = fields.Integer(string="Width, max(mm)", required=True)

    height_min = fields.Integer(string="Height, min(mm)", required=True)
    height_max = fields.Integer(string="Height, max(mm)", required=True)

    volume = fields.Float(string="Volume, max(m3)", digits=(6, 3), required=True)
    weight = fields.Integer(string="Weight(kg)", required=True)
    # Temperatures
    temp_min = fields.Integer(string="Working temperature, min(C)", required=True)
    temp_max = fields.Integer(string="Working temperature, max(C)", required=True)
    # Request for new product
    np_id = fields.Many2one(string="Product design request id", comodel_name='dm.bp.np')
    np_required = fields.Boolean(string="Specify product design request", compute="_dummy",
                                 default=_get_np_required)
    # Documents
    documents = fields.Many2many(string="Documents", comodel_name='ir.attachment',
                                 compute="get_documents",
                                 inverse="set_documents")
    # Appius integration
    appius_id = fields.Char(string="Appius ID")

    # Notes
    note = fields.Text(string="Note", translate=True)

    # Order
    _order = 'id'

    # SQL constraints
    _sql_constraints = [('grapple_variant_product_unique',
                         'UNIQUE (product)',
                         _('This grapple variant_already exists!'))]

# -- Dummy
    def _dummy(self):
        return

# -- Create -- #
    @api.model
    def create(self, vals):
        # Check if created from Request for new product add ref to np_id
        np_id = False
        if 'np_id' in vals:
            np_id = vals['np_id']

        # Create
        rec = super(DMGrappleVariant, self).create(vals)

        # Create NP line
        if np_id:
            ref = rec._name + "," + str(rec.id)
            self.env['dm.bp.np.line'].create({'np_id': np_id, 'dm_product_ref': ref})

        return rec

# -- Check parent
    @api.multi
    def check_parent(self):
        has_parent = True if self._context.get('default_grapple', False) else False
        for rec in self:
            rec.has_parent = has_parent


# -- Magnet changed
    @api.onchange('grapple')
    @api.multi
    def grapple_change(self):
        self.ensure_one()
        self.has_teeth = self.grapple.has_teeth
        self.name_draft = self.grapple.name
        self.weight = self.grapple.weight
        self.volume = self.grapple.volume
        self.length_min = self.grapple.length_min
        self.length_max = self.grapple.length_max
        self.width_min = self.grapple.width_min
        self.width_max = self.grapple.width_max
        self.height_min = self.grapple.height_min
        self.height_max = self.grapple.height_max
        self.temp_min = self.grapple.temp_min
        self.temp_max = self.grapple.temp_max

# -- Get documents
    @api.multi
    def get_documents(self):
        for rec in self:
            rec.documents = self.env['ir.attachment'].search(['&', ("res_model", '=', 'dm.grapple.variant'),
                                                              ('res_id', '=', rec.id)])

# -- Set documents
    @api.multi
    def set_documents(self):
        for rec in self:
            for document in rec.documents:
                if not document.res_id:
                    document.write({'res_model': 'dm.grapple.variant', 'res_id': rec.id})

# -- Compose name -- #
    @api.multi
    def compose_name(self):
        for rec in self:
            rec.name = rec.product_name if rec.product else rec.name_draft
