from openerp import models, fields, api, _
from openerp.exceptions import ValidationError
import re


# import logging
# Logger for debug
# _logger = logging.getLogger(__name__)


############
#  Partner #
############
class DMPartner(models.Model):
    _inherit = "res.partner"

# -- Get pricelist
    @api.multi
    def _get_pricelist(self):
        self.ensure_one()
        return self.env['res.users'].browse([self._uid]).company_id.pricelist_default.id

# -- Set lead manager
    @api.multi
    def _get_lead_manager(self):
        is_lead_manager = self.env['res.users'].has_group('dimet_erp.dm_lead_manager')
        res = self._uid if is_lead_manager else False
        return res

# -- Fields
    country_code = fields.Char(string="Country Code", related='country_id.code')
    phone = fields.Char(readonly=True, compute='_dummy')
    mobile = fields.Char(readonly=True, compute='_get_phone_number')
    fax = fields.Char(readonly=True, compute='_dummy')
    property_product_pricelist = fields.Many2one(string="Sale pricelist", comodel_name='product.pricelist',
                                                 required=True, default=_get_pricelist)
    user_id = fields.Many2one(default=lambda self: self._uid)
    section_id = fields.Many2one(default=lambda self: self.env['res.users'].browse([self._uid]).default_section_id.id)
    # TODO MUST delete default action in "Technical->Setting->User-defined actions"!
    lang = fields.Selection(default=lambda self: self.env['res.users'].browse([self._uid]).lang)
    opportunity_ids = fields.One2many(domain=[])

    # Country related fields
    inn_d = fields.Char(string="INN", size=14)
    kpp_d = fields.Char(string="KPP", size=9)

    # Counters
    opportunity_count = fields.Integer(compute="_new_opp_count")
    sale_order_count = fields.Integer(compute="_new_sale_order_count")

    # New fields
    phone_numbers = fields.One2many('avestrus.phone.number', 'partner_id', 'Phone numbers')
    lead_manager = fields.Many2one(string="Lead manager", comodel_name='res.users',
                                   default=_get_lead_manager)

# -- Create
    @api.model
    def create(self, vals):

        # Subscribe lead manager and sales person
        sales_person_id = False
        subscribe_ids = []

        val = vals.get('user_id', False)
        if val:
            partner = self.env['res.users'].sudo().browse([val]).partner_id
            if partner:
                sales_person_id = partner.id
                subscribe_ids.append([4, sales_person_id])

        val = vals.get('lead_manager', False)
        if val:
            partner = self.env['res.users'].sudo().browse([val]).partner_id
            if partner:
                lead_manager_id = partner.id
                if not lead_manager_id == sales_person_id:
                    subscribe_ids.append([4, lead_manager_id])

        if len(subscribe_ids) > 0:
            vals.update({'message_follower_ids': subscribe_ids})

        return super(DMPartner, self).create(vals)

# -- Write
    @api.multi
    def write(self, vals):

        # Subscribe lead manager and sales person
        sales_person_id = False
        subscribe_ids = []

        val = vals.get('user_id', False)
        if val:
            partner = self.env['res.users'].sudo().browse([val]).partner_id
            if partner:
                sales_person_id = partner.id
                subscribe_ids.append([4, sales_person_id])

        val = vals.get('lead_manager', False)
        if val:
            partner = self.env['res.users'].sudo().browse([val]).partner_id
            if partner:
                lead_manager_id = partner.id
                if not lead_manager_id == sales_person_id:
                    subscribe_ids.append([4, lead_manager_id])

        if len(subscribe_ids) > 0:
            vals.update({'message_follower_ids': subscribe_ids})

        res = super(DMPartner, self).write(vals)

        # Subscribe to partner's leads/opportunities and Sales Orders
        if len(subscribe_ids) > 0:
            for rec in self:
                rec_id = rec.id
                self.env['crm.lead'].sudo().search([('partner_id', '=', rec_id)]).sudo().write(
                    {'message_follower_ids': subscribe_ids}
                )
                self.env['sale.order'].sudo().search(
                    ['|', '|', ('partner_id', '=', rec_id), ('partner_invoice_id', '=', rec_id),
                     ('partner_shipping_id', '=', rec_id)]
                ).sudo().write(
                    {'message_follower_ids': subscribe_ids}
                )

        return res



# -- Odoo constraints
    @api.constrains('inn_d')
    @api.multi
    def _check_description(self):
        self.ensure_one()
        # Search companies with the same INN
        if not self.inn_d:
            return
        has_inn = self.env['res.partner'].sudo().search_count([('inn_d', '=', self.inn_d)])
        if has_inn > 1:
            raise ValidationError(_("Company with such INN already exists!"))

# -- Get all phones
    @api.depends('phone_numbers')
    @api.multi
    def _get_phone_number(self):
        for rec in self:
            phone_list = self.env['avestrus.phone.number'].search(
                [('partner_id', '=', rec.id), ('number_type', 'in', ['main', 'mobile', 'fax'])])
            if len(phone_list) > 0:
                for phone in phone_list:
                    if phone.number_type == 'main':
                        rec.phone = phone.number_unformatted
                    if phone.number_type == 'mobile':
                        rec.mobile = phone.number_unformatted
                    if phone.number_type == 'fax':
                        rec.fax = phone.number_unformatted

# -- INN changed
    @api.onchange('inn_d')
    @api.multi
    def onchange_inn_d(self):
        self.ensure_one()
        inn_d = self.inn_d or False
        if inn_d:
            if len(inn_d) > 1:
                self.inn_d = re.sub("[^0-9]", "", inn_d)

# -- KPP changed
    @api.onchange('kpp_d')
    @api.multi
    def onchange_kpp_d(self):
        self.ensure_one()
        kpp_d = self.kpp_d or False
        if kpp_d:
            if len(kpp_d) > 1:
                self.kpp_d = re.sub("[^0-9]", "", kpp_d)


# -- Company change
    @api.onchange('user_id')
    @api.multi
    def _change_company(self):
        self.ensure_one()
        self.property_product_pricelist = self.env['res.users'].browse([self._uid]).company_id.pricelist_default.id

# -- Dummy
    @api.multi
    def _dummy(self):
        return

# -- Count Sales Orders. Count all Sales Orders, even those has no access to
    @api.multi
    def _new_sale_order_count(self):
        for rec in self:
            if rec.is_company:
                operator = 'child_of'
            else:
                operator = '='

            rec.sale_order_count = self.env['sale.order'].sudo().search_count(
                ['|', ('partner_id', operator, rec.id), ('id', 'in', rec.sale_order_ids.ids)])

# -- Count Opportunities. Count even those has no access to
    @api.multi
    def _new_opp_count(self):
        for rec in self:
            if rec.is_company:
                operator = 'child_of'
            else:
                operator = '='

            current_id = rec.id
            rec.opportunity_count = self.env['crm.lead'].sudo().search_count(
                ['&', ('partner_id', operator, current_id), ('type', '=', 'opportunity')])

            rec.meeting_count = self.env['calendar.event'].sudo().search_count(
                ['|', ('partner_id', operator, current_id), ('attendee_ids', 'in', [current_id])])
