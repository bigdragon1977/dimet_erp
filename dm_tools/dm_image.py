try:
    import cStringIO as StringIO
except ImportError:
    import StringIO

from PIL import Image
from PIL import ImageEnhance
from random import randint
"""
Dimeterp tools
extended image tool Odoo with custom resize image from field model
"""
def image_resize_and_sharpen(image, size, preserve_aspect_ratio=False, factor=2.0):
 
        if image.mode != 'RGBA':
            image = image.convert('RGBA')
        image.thumbnail(size, Image.ANTIALIAS)
        if preserve_aspect_ratio:
            size = image.size
        sharpener = ImageEnhance.Sharpness(image)
        resized_image = sharpener.enhance(factor)
        # create a transparent image for background and paste the image on it
        image = Image.new('RGBA', size, (255, 255, 255, 0))
        image.paste(resized_image, ((size[0] - resized_image.size[0]) / 2, (size[1] - resized_image.size[1]) / 2))
        return image

def image_resize_image(base64_source, size=(1024, 1024), encoding='base64', filetype=None, avoid_if_small=False):
        if not base64_source:
            return False
        if size == (None, None):
            return base64_source
        image_stream = StringIO.StringIO(base64_source.decode(encoding))
        image = Image.open(image_stream)
        # store filetype here, as Image.new below will lose image.format
        filetype = (filetype or image.format).upper()

        filetype = {
            'BMP': 'PNG',
        }.get(filetype, filetype)

        asked_width, asked_height = size
        if asked_width is None:
            asked_width = int(image.size[0] * (float(asked_height) / image.size[1]))
        if asked_height is None:
            asked_height = int(image.size[1] * (float(asked_width) / image.size[0]))
        size = asked_width, asked_height

        # check image size: do not create a thumbnail if avoiding smaller images
        if avoid_if_small and image.size[0] <= size[0] and image.size[1] <= size[1]:
            return base64_source

        if image.size != size:
            image = image_resize_and_sharpen(image, size)
        if image.mode not in ["1", "L", "P", "RGB", "RGBA"]:
            image = image.convert("RGB")

        background_stream = StringIO.StringIO()
        image.save(background_stream, filetype)
        return background_stream.getvalue().encode(encoding)

def image_resize_image_small(base64_source, size=(250, 250), encoding='base64', filetype=None, avoid_if_small=False):
        return image_resize_image(base64_source, size, encoding, filetype, avoid_if_small)

def image_get_resized_images(base64_source,size=(250, 250), return_big=False, return_medium=True, return_small=True,
        big_name='image', medium_name='image_medium', small_name='image_small',
        avoid_resize_big=True, avoid_resize_medium=False, avoid_resize_small=False):
            return_dict = dict()
            if return_small:
                return_dict[small_name] = image_resize_image_small(base64_source,size, avoid_if_small=avoid_resize_small)
            return return_dict