from openerp import models, fields, api, _
import logging
# Logger for debug
_logger = logging.getLogger(__name__)


# -- Compose domain
def compose_domain(state):
    domain = "[('state','=','" + state + "')]"
    return domain


############
# ir.model #
############
class DMIrModel(models.Model):
    _name = 'ir.model'
    _inherit = 'ir.model'

    # Fields
    name = fields.Char(translate=True)
    access_rules = fields.One2many(string="Record access rules", comodel_name='ir.rule',
                                   inverse_name='model_id')


###########
# ir.rule #
###########
class DMIrRule(models.Model):
    _name = 'ir.rule'
    _inherit = 'ir.rule'

    # Fields
    dm_access_rules = fields.One2many(string="Dimet access rules", comodel_name='dm.access.rule',
                                      inverse_name='ir_rule')


##############
# res.groups #
##############
class DMResGroups(models.Model):
    _name = 'res.groups'
    _inherit = 'res.groups'

    # Fields
    name = fields.Char(translate=True)
    dm_access_rules = fields.Many2many(string="Groups", comodel_name='dm.access.rule',
                                       relation='dm_acc_rule_group_rel',
                                       column2='acc_rule',
                                       column1='group_id')


################
# Access rules #
################
class DMAccessRule(models.Model):
    _name = 'dm.access.rule'
    _description = "Dimet Access Rule"

    # Fields
    process = fields.Many2one(string="Business process", comodel_name='dm.process', required=False, ondelete='cascade')
    name = fields.Char(string="Name", related='process.name')
    odoo_model_id = fields.Many2one(string="Odoo model", comodel_name='ir.model',
                                    related='process.model_id', ondelete='cascade')
    state = fields.Selection([
        ('0', 'Draft'),
        ('1', 'Confirmed'),
        ('a', 'Accepted'),
        ('p', 'In progress'),
        ('s', 'Suspended'),
        ('e', 'Complete'),
        ('l', 'Declined'),
        ('c', 'Cancelled'),
        ('d', 'Done'),
    ], string="State", required=True)
    groups = fields.Many2many(string="Groups", comodel_name='res.groups',
                              relation='dm_acc_rule_group_rel',
                              column1='acc_rule',
                              column2='group_id')
    perm_read = fields.Boolean(string="For Read")
    perm_write = fields.Boolean(string="For Write")
    perm_create = fields.Boolean(string="For Create")
    ir_rule = fields.Many2one(string="Odoo rule ID", comodel_name='ir.rule', ondelete='cascade')

    # SQL constraints
    _sql_constraints = [('dm_access_rule_unique',
                         'UNIQUE (process, state, hr_department)',
                         _('This rule conflicts with existing!'))]


# -- Unlink --#
    @api.multi
    def unlink(self):
        _logger.info("Deleting dm_access_rules !!!")
        # Get ir_rule ids of deleted records
        ir_rule_ids = []
        for rec in self:
            ir_rule_ids.append(rec.ir_rule.id)

        # Call original function
        res = super(DMAccessRule, self).unlink()

        # Delete associated records from ir.rule
        ir_rules = self.env['ir.rule'].browse(ir_rule_ids)
        _logger.info("Deleting rules %s", ir_rule_ids)
        ir_rules.sudo().unlink()
        return res

