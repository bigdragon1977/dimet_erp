from openerp import models, fields, api, _
import logging
# Logger for debug
_logger = logging.getLogger(__name__)


######################
# CRM Claim Category #
######################
class DMCRMClaimCategory(models.Model):
    _inherit = "crm.case.categ"

    user_id = fields.Many2one(string="Default responsible", comodel_name='res.users')
    follower_ids = fields.Many2many(string="Default followers", comodel_name='res.users',
                                    relation='dm_crm_case_categ_user',
                                    column1='crm_case_categ',
                                    column2='user_id')


#############
# CRM Claim #
#############
class DMCRMClaim(models.Model):
    _inherit = "crm.claim"

# -- Fields
    categ_id = fields.Many2one(required=True)
    user_id = fields.Many2one(default=False)
    sales_order = fields.Many2one(string="Sales Order", comodel_name='sale.order')
    serial_number = fields.Many2one(string="Serial Number", comodel_name='stock.production.lot')
    type_action = fields.Selection([
        ('correction', 'Corrective Action'),
        ('prevention', 'Preventive Action'),
        ('combined', 'Combined Action'),
    ])

# -- Claim category change
    @api.onchange('categ_id')
    @api.multi
    def categ_id_change(self):
        for rec in self:
            rec.user_id = rec.categ_id.user_id

# -- Create
    @api.model
    def create(self, vals):

        # Add default followers
        # Get default followers for category
        if 'categ_id' not in vals:
            return super(DMCRMClaim, self).create(vals)

        default_follower_ids = self.env['crm.case.categ'].sudo().browse([vals['categ_id']]).follower_ids
        if len(default_follower_ids) < 1:
            return super(DMCRMClaim, self).create(vals)
        follower_list = []
        for follower in default_follower_ids:
            follower_list.append(follower.partner_id.id)

        if 'message_follower_ids' in vals:
            if vals['message_follower_ids']:
                vals['message_follower_ids'] += follower_list
            else:
                vals['message_follower_ids'] = follower_list

        return super(DMCRMClaim, self).create(vals)

    @api.multi
    def write(self, vals):
        _logger.info("Write vals before %s", vals)

        # Subscribe person task assigned to if not already assigned
        if 'categ_id' in vals and vals['categ_id']:
            default_user_ids = self.env['crm.case.categ'].sudo().browse([vals['categ_id']]).follower_ids
            if default_user_ids:
                default_follower_ids = []
                for default_user in default_user_ids:
                    default_follower_ids.append(default_user.partner_id.id)
                for rec in self:
                    vals['message_follower_ids'] = rec.message_follower_ids.ids + default_follower_ids

        _logger.info("Write vals after %s", vals)
        return super(DMCRMClaim, self).write(vals)
