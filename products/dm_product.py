from openerp import models, fields, api, _
# import logging
# Logger for debug
# _logger = logging.getLogger(__name__)


######################
# Dimet Base Product #
######################

# Used to store data common for all Dimet products

class DMProductBase(models.Model):
    _name = 'dm.product.base'

    # -- Check if np id is required
    @api.multi
    def _get_np_required(self):
        params = self._context.get('params')
        if params:
            np_required = False if 'action' in params else True
        else:
            np_required = True
        return np_required

    # Fields
    parent_id = fields.Many2one(string="Parent product", comodel_name='dm.product.base')
    children = fields.One2many(string="Child products", comodel_name='dm.product.base', inverse_name='parent_id')
    children_count = fields.Integer(string="Children count", compute="count_children")
    # Product.Product
    product = fields.Many2one(string="Related product", comodel_name='product.product')
    product_name = fields.Char(string="Name", related='product.name')
    # Name
    name = fields.Char(string="Name", compute='compose_name')
    name_draft = fields.Char(string="Name (no product)", translate=True)
    # Tech specs
    length = fields.Integer(string="Length(mm)", required=True)
    width = fields.Integer(string="Width(mm)", required=True)
    height = fields.Integer(string="Height(mm)", required=True)
    weight = fields.Integer(string="Weight(kg)", required=True)
    # Temperatures
    temp_min = fields.Integer(string="Working temperature, min(C)", required=True)
    temp_max = fields.Integer(string="Working temperature, max(C)", required=True)
    # Power and capacity
    # Request for new product
    np_id = fields.Many2one(string="Product design request id", comodel_name='dm.bp.np')
    np_required = fields.Boolean(string="Specify product design request", compute="_dummy",
                                 default=_get_np_required)
    # Documents
    documents = fields.Many2many(string="Documents", comodel_name='ir.attachment',
                                 compute="get_documents", inverse="set_documents")

    # SQL constraints
    _sql_constraints = [('dm_base_product_unique',
                         'UNIQUE (product)',
                         _('This product already exists!'))]

# -- Dummy
    def _dummy(self):
        return

# -- Name compose -- #
    @api.multi
    def compose_name(self):
        for rec in self:
            rec.name = rec.product_name if rec.product else rec.name_draft

# -- Get documents
    @api.multi
    def get_documents(self):
        for rec in self:
            rec.documents = self.env['ir.attachment'].search(['&', ("res_model", '=', _name),
                                                              ('res_id', '=', rec.id)])

# -- Set documents
    @api.multi
    def set_documents(self):
        for rec in self:
            for document in rec.documents:
                if not document.res_id:
                    document.write({'res_model': _name, 'res_id': rec.id})

# -- Count children
    @api.multi
    def count_children(self):
        for rec in self:
            rec.children_count = self.env['dm.product.base'].search_count([('parent_id', '=', rec.id)])

# -- Open children
    @api.multi
    def open_children(self):
        self.ensure_one()
        current_product_id = self.id
        context = {
            'default_parent_id': current_product_id
        }

        # Compose view header
        header = _("Children of ") + self.name

        return {
            'name': header,
            "views": [[False, "tree"], [False, "form"]],
            'res_model': self._name,
            'type': 'ir.actions.act_window',
            'target': 'current',
            'context': context,
            'domain': [('parent_id', '=', current_product_id)]
        }


#################################
# Dimet Product (for reference) #
#################################
class DMProduct(models.Model):
    _name = 'dm.product'

    # Fields
    # Basic
    name = fields.Char(string="Name", required=True, translate=True)
    model_id = fields.Many2one(string="Model", comodel_name='ir.model', required=True)
    model_name = fields.Char(string="Model", related='model_id.name')
    # Relations
    npd_ids = fields.Many2many(string="Product designs requested product", comodel_name='dm.bp.np',
                               relation='dm_product_np_rel',
                               column1='product',
                               column2='npd')

    # Order
    _order = 'name'

    # SQL constraints
    _sql_constraints = [('dm_product_unique',
                         'UNIQUE (name)',
                         _('This product already exists!'))]

# -- Update data from model to product.product
    @api.multi
    def update_product_data(self):
        for rec in self:
            items = self.env[rec.model_id.model].search([])
            for item in items:
                if item.product:
                    item_weight = item.weight
                    item.product.write({
                        'weight_net': item_weight,
                        'weight': item_weight
                    })


###############################
# Product IP protection class #
###############################
class DMProductIP(models.Model):
    _name = 'dm.product.ip'

    # Fields
    # Basic
    name = fields.Char(string="Name", required=True, translate=True)

    # Order
    _order = 'name'

    # SQL constraints
    _sql_constraints = [('dm_product_ip_unique',
                         'UNIQUE (name)',
                         _('This IP protection class already exists!'))]

