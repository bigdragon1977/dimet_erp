from openerp import models, fields, api, _
import logging
# Logger for debug
_logger = logging.getLogger(__name__)

#################
# Calendar event
#################
class DmCalendarEvent(models.Model):
    _inherit = "calendar.event"

    #   Fields
    state = fields.Selection(selection_add=[
        ('done', 'Is held'),
        ('c', 'Is cancelled')], readonly=True)
    alarm_ids = fields.Many2many(states={'done': [('readonly', True)]})
    description = fields.Text(states={'done': [('readonly', True)]})
    report_note = fields.Text(string="Report")
    expense_report = fields.Many2many(string="Expense report", comodel_name='hr.expense.expense',
                                      relation='dm_event_expense',
                                      column1='expense_report_id',
                                      column2='event_id')

    # Add partner
    partner_id = fields.Many2one(string="Partner", comodel_name='res.partner', required=True)

    # -- ###############
    # -- Confirm meeting
    @api.multi
    def confirm_meeting(self):
        for rec in self:
            rec.state = 'open'
        return

    # -- ###############
    # -- Cancel meeting
    @api.multi
    def cancel_meeting(self):
        for rec in self:
            rec.state = 'c'
        return

    # -- ####################
    # -- Mark meeting as held
    @api.multi
    def hold_meeting(self):
        self.ensure_one()
        self.write({'state': 'done'})
        return


# -- Write
    @api.multi
    def write_temp(self, vals):  # TODO add partner as attendee if not in list
        # Add partner to attendees lis if not already in
        _logger.info("Vals before %s", vals)
        partner_id = vals['partner_id'] if 'partner_id' in vals else self[0].partner_id.id
        if 'attendee_ids' in vals:
                vals['attendee_ids'].append([4, partner_id])

        _logger.info("Vals after %s", vals)
        res = super(DmCalendarEvent, self).write(vals)
        return res
